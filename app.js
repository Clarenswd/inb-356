/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
 
var natural = require('natural'),
    TfIdf = natural.TfIdf,
    tfidf = new TfIdf();

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('secret keywords'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.session({
	secret: "very secret"
}));
 
//this is the login 
app.get('/', function(req,res){
    res.render('login');
});
 
 
 

http.createServer(app).listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
});

var util = require('util');

var OAuth = require('oauth').OAuth;
 

// Claren's keys;
/*  
var oa = new OAuth(
	"https://api.twitter.com/oauth/request_token",
	"https://api.twitter.com/oauth/access_token",
	"D582yAVMG5HQpHumgdKdG9dpM",
	"Sr1K97D7nQsplELNRkdUcDNZEZFILABxxBfau0SuOkAyxoMG7M",
	"1.0",
	"http://localhost:3000/auth/twitter/callback",
	"HMAC-SHA1"
);
*/
// initial keys
var oa = new OAuth(
	"https://api.twitter.com/oauth/request_token",
	"https://api.twitter.com/oauth/access_token",
	"A6x1nzmmmerCCmVN8zTgew",
	"oOMuBkeqXLqoJkSklhpTrsvuZXo9VowyABS8EkAUw",
	"1.0",
	"http://localhost:3000/auth/twitter/callback",
	"HMAC-SHA1"
);
app.get('/auth/twitter', function(req, res) {
	oa.getOAuthRequestToken(function(error, oauth_token, oauth_token_secret, results) {
		if (error) {
			//Go back to Login screen
            res.redirect('/');        
		} else {
			req.session.oauth = {};
			req.session.oauth.token = oauth_token;
			console.log('oauth.token: ' + req.session.oauth.token);
			req.session.oauth.token_secret = oauth_token_secret;
			console.log('oauth.token_secret: ' + req.session.oauth.token_secret);
			res.redirect('https://twitter.com/oauth/authenticate?oauth_token=' + oauth_token)
		}
	});
});

var twitter = require('ntwitter');
app.get('/auth/twitter/callback', function(req, res, next) {
	if (req.session.oauth) {
		req.session.oauth.verifier = req.query.oauth_verifier;
		var oauth = req.session.oauth;

		oa.getOAuthAccessToken(oauth.token, oauth.token_secret, oauth.verifier,
			function(error, oauth_access_token, oauth_access_token_secret, results) {
				if (error) {
					console.log(error);
					res.send("Something broke.");
				} else {
					req.session.oauth.access_token = oauth_access_token;
					req.session.oauth.access_token_secret = oauth_access_token_secret;
					//console.log(results);
					//console.log(req);
					var twit = new twitter({
						consumer_key: "D582yAVMG5HQpHumgdKdG9dpM",
						consumer_secret: "Sr1K97D7nQsplELNRkdUcDNZEZFILABxxBfau0SuOkAyxoMG7M",
						access_token_key: req.session.oauth.access_token,
						access_token_secret: req.session.oauth.access_token_secret
					});
                    res.redirect('/home');


				 

				}
			}
		);
	} else
		next(new Error("you're not supposed to be here."))
});


app.get('/home', routes.index);
app.get('/home/:track', routes.index);
