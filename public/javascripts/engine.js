function TestApp($scope) {
    $scope.count = 1;
    $scope.countLove = 0;
    $scope.countHate = 0;
    $scope.countMiddle = 0;
    $scope.graphobj = "10";
    $scope.showStream = true;
    $scope.twitts = [];
    var socket = io.connect('http://localhost:3001');
    window.socket = socket;
    socket.on('newTwitt', function(item) {
        $scope.twitts.push(item);

        if ((item.text.indexOf('amor') != -1 || item.text.indexOf('love') != -1) &&
            (item.text.indexOf('odio') != -1 || item.text.indexOf('hate') != -1)) {
            $scope.countMiddle++;
        } else if (item.text.indexOf('amor') != -1 || item.text.indexOf('love') != -1) {
            $scope.countLove++;
            item.color = '#000';
        } else {
            $scope.countHate++;
            item.color = '#000';
        }
        //console.log(item);
        if ($scope.twitts.length > 30)
            $scope.twitts.splice(0, 1);
        if ($scope.showStream) {
            $scope.$apply();
        }

    });



    var theKeys = [];
    var ulKeyList = $('#keywordList');
    var f1_btn = $('#f1_btn');
    var f2_btn = $('#f2_btn');
    var f3_btn = $('#f3_btn');
    var f4_btn = $('#f4_btn');
    f2_btn.hide();
    $("#f2_btn").prop('disabled', true);
    f3_btn.hide();
    f4_btn.hide();
    var x;
    var theKeyword = $('#theKeyword');

    f1_btn.on('click', function() {
        //ulKeyList.append('<li>'+theKeyword.val()+'</li>');
        theKeys = theKeyword.val().split(" ");
        socket.emit('connectionx', theKeys);
        $(this).hide();
        theKeyword.val("");
        f2_btn.show();
    });

    f2_btn.on('click', function() {
        //ulKeyList.append('<li>'+theKeyword.val()+'</li>');
        //theKeys = theKeyword.val().split(" ");
        socket.emit('connectiony', theKeys);
        $(this).hide();
        f3_btn.show();
        f4_btn.show();
        x.hide();
        d3.hide();
    });

    f3_btn.on('click', function() {
        $scope.showStream = false;

    });
    f4_btn.on('click', function() {
        $scope.showStream = true;

    });

    socket.on('graph', function(graphdata) {
        $("#f2_btn").prop('disabled', false);
        var data = [];
        for (var i = 0; i < graphdata.length; i++) {
            data.push(graphdata[i].tfIdf);
            theKeys.push(graphdata[i].word)
        }
        var iteration = -1;
        x = d3.scale.linear()
            .domain([0, d3.max(data)])
            .range([0, 420]);

        d3.select(".chart")
            .selectAll("div")
            .data(data)
            .enter().append("div")
            .style("width", function(d) {
                return x(d) + "px";
            })
            .text(function(d) {
                iteration++;
                return d + " - " + graphdata[iteration].word;
            });

        $scope.$apply();
    });
}
