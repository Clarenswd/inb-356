/*
 * GET home page.
 */
var twitter = require('ntwitter');
var io = require('socket.io').listen(3001, {
    log: false
});
var isActive = false;
var initKeywords = [];
exports.index = function(req, res) {
    res.render('index', {
        title: 'INX-56|Project'
    });


    if (req.session.oauth) {
        var twit = new twitter({
            //---------------------------------------------------------------------
            //Clarens keys:
            //consumer_key: "D582yAVMG5HQpHumgdKdG9dpM",
            //consumer_secret: "Sr1K97D7nQsplELNRkdUcDNZEZFILABxxBfau0SuOkAyxoMG7M",
            //---------------------------------------------------------------------
            // initial keys:
            consumer_key: "A6x1nzmmmerCCmVN8zTgew",
            consumer_secret: "oOMuBkeqXLqoJkSklhpTrsvuZXo9VowyABS8EkAUw",
            access_token_key: req.session.oauth.access_token,
            access_token_secret: req.session.oauth.access_token_secret
        });
        var twit1 = new twitter({
            //---------------------------------------------------------------------
            //Clarens keys:
            //consumer_key: "D582yAVMG5HQpHumgdKdG9dpM",
            //consumer_secret: "Sr1K97D7nQsplELNRkdUcDNZEZFILABxxBfau0SuOkAyxoMG7M",
            //---------------------------------------------------------------------
            // initial keys:
            consumer_key: "A6x1nzmmmerCCmVN8zTgew",
            consumer_secret: "oOMuBkeqXLqoJkSklhpTrsvuZXo9VowyABS8EkAUw",
            access_token_key: req.session.oauth.access_token,
            access_token_secret: req.session.oauth.access_token_secret
        });
        io.on('connection', function(socket) {
            console.log("a connection");
            socket.on('connectionx', function(data) {
                isActive = false;
                counterTweets = 0
                initKeywords = data;
                InitStream1(twit, data);

            });
            socket.on('connectiony', function(data) {
                isActive = false;
                var keyWordsOfInterest = initKeywords.concat(data);
                InitStream2(twit1, keyWordsOfInterest);

            });


        });

    } else {
        res.redirect('/');
    }
};



//----------------------------------------------------------------
//------------------------variables for Classifier-------------
//----------------------------------------------------------------
var LanguageDetect = require('languagedetect');
var lngDetector = new LanguageDetect();
//
var natural = require('natural'),
    TfIdf = natural.TfIdf,
    stemmer = natural.PorterStemmer;
stemmer.attach();

var tfIdf = new TfIdf();

var counterTweets = 0;

var readyToClassify = true;
var tweetsToAnalyse = [];
var tweetsToAnalyseWithoutDuplicates = [];
var allWords = [];
var tweetsToAnalyseArrOfArray = [];

var stemToItsWord = [];
var stemToTFIDF = [];
var stemToTFIDFSorted = [];
//----------------------------------------------------------------
//------------------------END variables for Classifier-------------
//----------------------------------------------------------------


//var InitStream1 = function(twit, data3) {
function InitStream1(twit, data3) {

    if (!isActive) {
        console.log('init Stream1');
        twit.stream('statuses/filter', {
                track: data3
            },
            function(stream) {
                stream.on('data', function(data) {
                    //console.log(":::: InitStream1");

                    if (counterTweets < 50) {
                        if (filterTweet(data.text)) {
                            console.log("normal tweet1: ");
                            io.sockets.emit('newTwitt', data);
                            stemToItsWord = stemToItsWord.concat(createStemToWordArray(data.text));
                            //console.log("createStemToWordArray finished");
                            tfIdf.addDocument(data.text.tokenizeAndStem());
                            tweetsToAnalyse.push(data.text.tokenizeAndStem());
                            counterTweets++;
                            // streams to the client:

                        } else {
                            // console.log("out      tweet1: " + data.text);
                        }

                    } else if (readyToClassify) {
                        readyToClassify = false;
                       // console.log("It finishes");

                        var newKeyWords = determineNewKeyWords(10);
                        io.sockets.emit('graph', newKeyWords);
                        //console.log(newKeyWords);
                    }

                });

                stream.on('end', function(b) {
                    console.log('end stream', b.toString());
                    isActive = true;

                });
                stream.on('destroy', function(b) {
                    console.log('destroy stream', b.toString());

                    isActive = true;

                });

            }
        );
        isActive = true;
    }
};

var counterTweets3 = 0;

var InitStream2 = function(twit, data3) {
    //console.log(":::: InitStream2 : " + data3);

    if (!isActive) {
        console.log('Start Stream2');
        twit.stream('statuses/filter', {
                track: data3
            },
            function(stream) {
                stream.on('data', function(data) {
                   // console.log(":::: InitStream2");

                    if (filterTweet(data.text)) {
                       // console.log("normal tweet2: ");
                        io.sockets.emit('newTwitt', data);
                        //stemToItsWord = stemToItsWord.concat(createStemToWordArray(data.text));
                        //console.log("createStemToWordArray finished");
                        //tfIdf.addDocument(data.text.tokenizeAndStem());
                        //tweetsToAnalyse.push(data.text.tokenizeAndStem());
                        //counterTweets3++;
                        // streams to the client:
                        // io.sockets.emit('newTwitt', data);
                    } else {
                        // console.log("out      tweet2: " + data.text);
                    }

                });

                stream.on('end', function(b) {
                    console.log('end stream', b.toString());
                    isActive = true;

                });
                stream.on('destroy', function(b) {
                    console.log('destroy stream', b.toString());
                    // counterTweets3 = 0;
                    isActive = true;

                });

            }
        );
        isActive = true;
    }
};

//----------------------------------------------------------------
//------------------------TF-IDF---------------------------
//----------------------------------------------------------------
function determineNewKeyWords(amount) {
    clasify();
    sortStemToTFIDF();
    //printArray();
    var newKeyWordsResult = getNewKeyWords(amount);
    //console.log(newKeyWordsResult);
    // var newFilteredKeyWords = filterNewKeyWords(newKeyWordsResult);
    //console.log(newKeyWords);
    return newKeyWordsResult;
}

function filterNewKeyWords(newKeyWords) {
    for (var i = 0; i < newKeyWords.length; i++) {
        // console.log('type of : ' + newKeyWords[i] + " is " + typeof newKeyWords[i]);
        var result = meaningfulWord(newKeyWords[i].word);

        if (!result) {
            //console.log('splice : ' + newKeyWords[i]);
            newKeyWords.splice(i, 1);
            i--;
        }
    }
    return newKeyWords;
}

function normalWordWithoutSpecialCharacter(w) {
    return /^[a-zA-Z]+$/.test(w);
}

function meaningfulWord(targetW) {
    // console.log('meaningfulWord called');

    if (typeof targetW == 'undefined') {
        //console.log('targetW == "undefined"' + targetW);
        return false;
    } else if (typeof targetW == undefined) {
        // console.log('targetW == undefined' + targetW);
        return false;
    } else if (typeof targetW == 'object') {
        // console.log("targetW == 'object'" + targetW);

        if (typeof targetW[0] == 'undefined') {
            //  console.log("targetW == 'object'+ targetW[0] == 'undefined'" + targetW[0]);
            return false;
        } else {
            //  console.log("else: targetW[0] ==" + targetW[0]);

        }
    } else if (!normalWordWithoutSpecialCharacter(targetW)) {
        return false;
    }

    return true;
}


function getNewKeyWords(amount) {
    //console.log("getNewKeyWords called.");
    var keyWordsResult = [];
    var word = "";
    if (amount > stemToTFIDFSorted.length) {
        amount = stemToTFIDFSorted.length;
    }
    for (var a = 0; a < stemToTFIDFSorted.length; a++) {
        //console.log(stemToTFIDFSorted[a].stem + " : " + stemToTFIDFSorted[a].tfIdf);
        //word = getWord(stemToTFIDFSorted[a].stem);

        var wordToPush = getWord(stemToTFIDFSorted[a].stem);
        //console.log(keyWordsResult);
        //console.log("word to push: " + wordToPush);
        if (meaningfulWord(wordToPush)) {
            if (!keyWordAlreadyExists(wordToPush, keyWordsResult)) {
                // var tmp = (Math.round(stemToTFIDFSorted[a].tfIdf * 10)/10).toFixed(4);
                // var tmp = Number(stemToTFIDFSorted[a].tfIdf).toFixed(4);
                //console.log(" -->  round: " +stemToTFIDFSorted[a].tfIdf + " : " + tmp);
                keyWordsResult.push({
                    word: wordToPush,
                    tfIdf: Number(stemToTFIDFSorted[a].tfIdf).toFixed(3)
                });
            }

        }
        if (keyWordsResult.length >= amount) {
            break;
        }
    }

    return keyWordsResult;
}

function keyWordAlreadyExists(word, array) {
    for (var y = 0; y < array.length; y++) {
        if (array[y].word == word) {
            return true;
        }
    }
    return false;
}

function getWord(stem) {
    //console.log("getWord called.");
    for (var b = 0; b < stemToItsWord.length; b++) {
        // console.log(stemToItsWord[b].word + " - " + stemToItsWord[b].stem + " == " + stem);
        if (stem == stemToItsWord[b].stem) {
            return stemToItsWord[b].word;
        }
    }
}

function printNormalArray(array) {
    //  console.log("  !!!!!!!!!! printArray              length : " + stemToItsWord.length);
    for (var i = 0; i < array.length; i++) {
        //console.log(array[i]);
    }

}

function printArray() {
    // console.log("  !!!!!!!!!! printArray              length : " + stemToItsWord.length);
    for (var i = 0; i < stemToItsWord.length; i++) {
        //console.log(stemToItsWord[i].stem + " -> " + stemToItsWord[i].word);
    }

}

function clasify() {
    //console.log("clasify called");

    tweetsToAnalyseDeleteDuplicates();

    for (var i = 0; i < tweetsToAnalyseWithoutDuplicates.length; i++) {
        //console.log(tweetsToAnalyse[i]);

        for (var j = 0; j < tweetsToAnalyseWithoutDuplicates[i].length; j++) {
            //console.log(tweetsToAnalyse[i][j]);
            var sum = 0;
            tfIdf.tfidfs(tweetsToAnalyseWithoutDuplicates[i][j], function(i, measure) {
                sum += measure;
                //console.log('document #' + i + ' is ' + measure);
            });
            //console.log(tweetsToAnalyseWithoutDuplicates[i][j]);
            //console.log(sum);
            stemToTFIDF.push({
                stem: tweetsToAnalyseWithoutDuplicates[i][j],
                tfIdf: sum
            });
        }
    }
}

function tweetsToAnalyseDeleteDuplicates() {
    //console.log("tweetsToAnalyseDeleteDuplicates called : ");
    tweetsToAnalyseWithoutDuplicates = tweetsToAnalyse.slice(0);

    for (var i = 0; i < tweetsToAnalyse.length; i++) {
        //console.log(tweetsToAnalyse[i]);
        for (var j = 0; j < tweetsToAnalyse[i].length; j++) {
            if (containsInTweetsToAnalyse(tweetsToAnalyse[i][j])) {

            }


        }

    }
}

function containsInTweetsToAnalyse(word) {
    //console.log("containsInTweetsToAnalyse called : ");
    for (var x = 0; x < tweetsToAnalyseWithoutDuplicates.length; x++) {
        for (var y = 0; y < tweetsToAnalyseWithoutDuplicates[x].length; y++) {
            if (word == tweetsToAnalyseWithoutDuplicates[x][y]) {
                tweetsToAnalyseWithoutDuplicates[x].splice(y, 1);
            }
        }

    }
}


var sortStemToTFIDF = function() {
    //console.log("sortStemToTFIDF called : ");
    var stemToTFIDFSortedtmp = stemToTFIDF.slice(0);
    stemToTFIDFSortedtmp.sort(function(a, b) {
        return b.tfIdf - a.tfIdf;
    });

    stemToTFIDFSorted = stemToTFIDFSortedtmp;
    //console.log(stemToTFIDFSorted);

};




function tf(term, tweet) {
    var amount = 0;
    for (var i = 0; i < tweet.length; i++) {
        //console.log( term + " == "+ tweet[i]);
        if (term == tweet[i]) {
            amount++;
        }
    }
    return amount;
}


//http delete, stopwords, if it is still there good after http
function preprocesse(tweet) {
    // take only enlgish, delete URLs
    return results
}


function filterTweet(dataText) {
    if (dataText.indexOf("http") == -1 && lngDetector.detect(dataText, 1)[0][0] == "english" && !RT(dataText)) {
        return true;
    } else {
        return false
    }
}


function filterStemms(stem) {
    if (typeof stem[0] == 'undefined') {
        return false;
    } else if (typeof stem == 'undefined') {
        return false;
    } else if (stem[0].length == 1 || stem[0].length == 2) {
        return false;
    }
    return true;
}

function createStemToWordArray(tweetTxt) {
    var stemToItsWord1 = [];

    var tweetArr = tweetTxt.split(" ");
    var stemValue = "";

    for (var i = 0; i < tweetArr.length; i++) {
        stemValue = tweetArr[i];
        //console.log("stemValue: " + stemValue);
        stemValue = stemValue.tokenizeAndStem();
        //console.log("tweetArr: " + tweetArr[i] + " stemValue : -" + stemValue + "- typeof " + typeof stemValue[0]);

        if (!contains(stemValue, stemToItsWord)) {
            if (filterStemms(stemValue)) {
                //console.log(" two filters pass stemToItsWord1 push ");
                stemToItsWord1.push({
                    stem: stemValue,
                    word: tweetArr[i]
                });
            }
        } else {
            //console.log(" doesnt contain in stemToITsWordd , stemValue[0].length: " + stemValue[0]);
        }
    }
    return stemToItsWord1;
}


function contains(stemValue, stemToItsWord2) {
    //console.log("contains called");
    for (var i = 0; i < stemToItsWord2.length; i++) {
        //console.log("contains: stemToItsWord2: " + stemToItsWord2[i].stem + " == " + stemValue[0]);
        if (stemToItsWord2[i].stem == stemValue[0]) {
            return true;
        }
    }
    return false;
}


function RT(dataText) {
    //console.log('dataText[0] ' + dataText[0]);
    if (dataText[0] == 'R' && dataText[1] == 'T') {
        return true;
    } else {
        return false;
    }


}
//----------------------------------------------------------------
//------------------------TF-IDF--------------------
//----------------------------------------------------------------